import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
	public empresas: any[];
	public oferta: any;

  constructor(public navCtrl: NavController, private http: Http) {
  	this.oferta = {};

  }
  ngOnInit(){
  	this.http.get('http://127.0.0.1:8000/api/empresas')
  		.map((res: Response) => {
  			let body = res.json();
  			return body || {};
  		})
  		.subscribe(empresas => this.empresas = empresas.empresas.data)
  }

  adicionarOferta(){
  	console.log(this.oferta)
  	this.http.post('http://127.0.0.1:8000/api/ofertas', this.oferta)
  		.toPromise()
  		.then(res => res.json());
  	this.navCtrl.push(HomePage);
  }

}
