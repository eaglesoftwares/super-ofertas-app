import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
	
	public ofertas = [];

  constructor(public navCtrl: NavController, private http: Http) {

  }

  ngOnInit(){
  	this.http.get('http://127.0.0.1:8000/api/ofertas')
  		.map((res: Response) => {
  			let body = res.json();
  			return body || {};
  		})
  		.subscribe(ofertas => this.ofertas = ofertas.ofertas)
  }
    doRefresh(refresher) {
    this.http.get('http://127.0.0.1:8000/api/ofertas')
  		.map((res: Response) => {
  			let body = res.json();
  			return body || {};
  		})
  		.subscribe(ofertas => this.ofertas = ofertas.ofertas)

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

}
